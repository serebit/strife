package com.serebit.strife.entities

import com.serebit.strife.BotClient
import com.serebit.strife.GetCacheData
import com.serebit.strife.data.AvatarData
import com.serebit.strife.data.PermissionOverride
import com.serebit.strife.internal.entitydata.*
import com.serebit.strife.internal.network.Route
import com.serebit.strife.internal.packets.toInvite
import io.ktor.http.*
import kotlinx.coroutines.flow.Flow
import kotlinx.datetime.Instant

/** Represents a text or voice channel within Discord. */
interface Channel : Entity {
    /**
     * The types of channels which bot clients can interact with.
     * @property id The ID int of the channel type,
     * [see](https://discordapp.com/developers/docs/resources/channel#channel-object-channel-types)
     */
    enum class Type(val id: Int) {
        /** [GuildTextChannel] */
        GUILD_TEXT(0),

        /** [DmChannel] */
        DM(1),

        /** [GuildVoiceChannel] */
        GUILD_VOICE(2),

        /** [GuildChannelCategory] */
        GUILD_CATEGORY(4),

        /** [GuildNewsChannel] */
        GUILD_NEWS(5),

        /** [GuildStoreChannel] */
        GUILD_STORE(6)
    }
}

/** A [Channel] used to send textual messages with optional attachments. */
interface TextChannel : Channel {
    /** Returns the last (most recent) message sent in this channel. */
    suspend fun getLastMessage(): Message?

    /**
     * Returns the message with the given ID or null if it was not found.
     *
     * @param messageID The ID of the requested message.
     */
    suspend fun getMessage(messageID: Long): Message?

    /** The date and time of the last time a message was pinned in this [TextChannel]. */
    suspend fun getLastPinTime(): Instant?

    /** Send an [Embed][EmbedBuilder] to this [TextChannel]. Returns the sent [Message] or null if not sent. */
    suspend fun send(embed: EmbedBuilder): Message?

    /**
     * Send a [Message] with [text] and an optional [embed] to this [TextChannel].
     * Returns the [Message] which was sent or null if it was not sent.
     */
    suspend fun send(text: String, embed: EmbedBuilder? = null): Message?

    /**
     * Send a file to this [TextChannel] with the given [name] and [data]. Returns the [MessageData] which was sent or
     * null if it was not sent.
     */
    suspend fun sendFile(name: String, data: ByteArray): Message?

    /** Show the bot client as `bot_name is typing...` beneath the text-entry box. Returns `true` if successful. */
    suspend fun sendTyping(): Boolean =
        context.requester.sendRequest(Route.TriggerTypingIndicator(id)).status.isSuccess()

    /**
     * Returns a flow of this channel's [Message]s with an optional [limit] and either [before] or [after]
     * @param before The message id to get messages before.
     * @param after The message id to get messages after.
     * @param limit The max number of messages to return. Whole history is returned if not specified.
     * */
    suspend fun flowOfMessages(before: Long? = null, after: Long? = null, limit: Int? = null): Flow<Message>
}

/** A Private Direct Message [TextChannel] used to talk with a single [User]. */
class DmChannel internal constructor(override val id: Long, override val context: BotClient) : TextChannel {
    private suspend fun getData() = context.obtainDmChannelData(id)
        ?: error("Attempted to get data for a nonexistent DM channel with ID $id")

    override suspend fun getLastMessage(): Message? =
        getData().getLastMessage()?.lazyEntity
            ?: context.requester.sendRequest(Route.GetChannelMessages(id, limit = 1)).value
                ?.firstOrNull()
                ?.let { context.cache.pushMessageData(it) }
                ?.lazyEntity

    override suspend fun getMessage(messageID: Long): Message? =
        context.cache.get(GetCacheData.Message(messageID, id))?.lazyEntity
            ?: context.requester.sendRequest(Route.GetChannelMessage(id, messageID)).value
                ?.let { context.cache.pushMessageData(it) }
                ?.lazyEntity

    override suspend fun getLastPinTime(): Instant? = getData().lastPinTime

    /** The [users][User] who have access to this [DmChannel]. */
    suspend fun recipient(): User? = getData().recipient?.lazyEntity

    override suspend fun send(embed: EmbedBuilder): Message? = getData().send(embed = embed)?.lazyEntity
    override suspend fun sendFile(name: String, data: ByteArray): Message? = getData().sendFile(name, data)?.lazyEntity
    override suspend fun send(text: String, embed: EmbedBuilder?): Message? = getData().send(text, embed)?.lazyEntity

    override suspend fun flowOfMessages(before: Long?, after: Long?, limit: Int?): Flow<Message> =
        getData().flowOfMessages(before, after, limit)

    /** Checks if this channel is equivalent to the [given object][other]. */
    override fun equals(other: Any?): Boolean = other is Entity && other.id == id
}

/**  A representation of any [Channel] which can only be found within a [Guild]. */
interface GuildChannel : Channel {
    /** The [Guild] housing this channel. */
    suspend fun getGuild(): Guild

    /** The sorting position of this channel in its [getGuild]. */
    suspend fun getPosition(): Int

    /** The displayed name of this channel in its [getGuild]. */
    suspend fun getName(): String

    /** Explicit [permission overrides][PermissionOverride] for members and roles. */
    suspend fun getPermissionOverrides(): List<PermissionOverride>

    /**
     * Create a new [Invite] for this [GuildChannel]. You can optionally specify details about the invite like
     * the [maximum age in seconds][ageLimit], the [maximum number of uses][useLimit], whether to grant [temporary]
     * membership, and whether this Invite must be [unique] (useful for creating many unique one time use invites).
     *
     * If an [Invite] is set to grant [temporary] membership, users will be removed from the [getGuild] when they
     * disconnect -- unless they have been assigned a [GuildRole].
     *
     * Returns the code of the newly created invite or `null` if one was not created.
     */
    suspend fun createInvite(
        ageLimit: Int = 86400,
        useLimit: Int = 0,
        temporary: Boolean = false,
        unique: Boolean = false
    ): String? =
        context.requester.sendRequest(Route.CreateChannelInvite(id, ageLimit, useLimit, temporary, unique)).value?.code

    /** Returns a list of [Invite]s associated with this [GuildChannel] or `null` if the request failed. */
    suspend fun getInvites(): List<Invite>? = context.requester.sendRequest(Route.GetChannelInvites(id)).value
        ?.map { ip ->
            ip.toInvite(
                context,
                getGuild(),
                getGuild().getMembers().firstOrNull { it.getUser().id == ip.inviter.id })
        }

    /** Returns the [Invite] with the given [code]. Returns `null` if the request fails or no [Invite] is found. */
    suspend fun getInvite(code: String): Invite? = getInvites()?.firstOrNull { it.code == code }
}

/** A [TextChannel] found within a [Guild]. */
interface GuildMessageChannel : TextChannel, GuildChannel {
    /** The topic displayed above the message window and next to the channel name (0-1024 characters). */
    suspend fun getTopic(): String

    /**
     * Whether this channel is marked as NSFW. NSFW channels have two main differences: users have to explicitly say
     * that they are willing to view potentially unsafe-for-work content via a prompt, and these channels are exempt
     * from [explicit content filtering][Guild.getExplicitContentFilter].
     */
    suspend fun isNsfw(): Boolean

    /** Get all webhooks that belong to this channel. Returns a list of webhooks, or `null` on failure. */
    suspend fun getWebhooks(): List<Webhook>?

    /**
     * Create a webhook in this channel with the given [name], and optionally an [avatar]. Returns the created webhook,
     * or `null` on failure.
     */
    suspend fun createWebhook(name: String, avatar: AvatarData? = null): Webhook?
}

/** A [TextChannel] found within a [Guild]. */
class GuildTextChannel internal constructor(override val id: Long, override val context: BotClient) :
    GuildMessageChannel, Mentionable {

    private suspend fun getData() = context.obtainGuildTextChannelData(id)
        ?: error("Attempted to get data for a nonexistent guild text channel with ID $id")

    override suspend fun asMention(): String = id.asMention(MentionType.CHANNEL)
    override suspend fun getName(): String = getData().name
    override suspend fun getGuild(): Guild = getData().guild.lazyEntity
    override suspend fun getPosition(): Int = getData().position.toInt()
    override suspend fun getPermissionOverrides(): List<PermissionOverride> =
        getData().permissionOverrides.values.toList()

    override suspend fun getLastMessage(): Message? =
        getData().getLastMessage()?.lazyEntity
            ?: context.requester.sendRequest(Route.GetChannelMessages(id, limit = 1)).value
                ?.firstOrNull()
                ?.let { context.cache.pushMessageData(it) }
                ?.lazyEntity

    override suspend fun getMessage(messageID: Long): Message? =
        context.cache.get(GetCacheData.Message(messageID, id))?.lazyEntity
            ?: context.requester.sendRequest(Route.GetChannelMessage(id, messageID)).value
                ?.let { context.cache.pushMessageData(it) }
                ?.lazyEntity


    override suspend fun getLastPinTime(): Instant? = getData().lastPinTime
    override suspend fun getTopic(): String = getData().topic
    override suspend fun isNsfw(): Boolean = getData().isNsfw

    /** A configurable per-user rate limit that defines how often a user can send messages in this channel. */
    suspend fun getRateLimitPerUser(): Int? = getData().rateLimitPerUser?.toInt()

    override suspend fun send(embed: EmbedBuilder): Message? = getData().send(embed = embed)?.lazyEntity
    override suspend fun sendFile(name: String, data: ByteArray): Message? = getData().sendFile(name, data)?.lazyEntity
    override suspend fun send(text: String, embed: EmbedBuilder?): Message? = getData().send(text, embed)?.lazyEntity

    override suspend fun flowOfMessages(before: Long?, after: Long?, limit: Int?): Flow<Message> =
        getData().flowOfMessages(before, after, limit)

    override suspend fun getWebhooks(): List<Webhook>? = context.requester.sendRequest(Route.GetChannelWebhooks(id))
        .value
        ?.map { it.toEntity(context, getData().guild, getData()) }

    override suspend fun createWebhook(name: String, avatar: AvatarData?): Webhook? = context.requester
        .sendRequest(Route.CreateWebhook(id, name, avatar))
        .value
        ?.toEntity(context, getData().guild, getData())

    /** Checks if this channel is equivalent to the [given object][other]. */
    override fun equals(other: Any?): Boolean = other is GuildTextChannel && other.id == id
}

/**
 * A channel that users can follow and crosspost into their own server.
 * News channels can be interacted with the same way [GuildTextChannel] can be.
 * News channels are only available to some verified guilds "for now" - Discord Devs.
 */
class GuildNewsChannel internal constructor(override val id: Long, override val context: BotClient) :
    GuildMessageChannel {

    private suspend fun getData() = (context.obtainGuildChannelData(id) as? GuildNewsChannelData)
        ?: error("Attempted to get data for a nonexistent guild news channel with ID $id")

    override suspend fun getName(): String = getData().name
    override suspend fun getGuild(): Guild = getData().guild.lazyEntity
    override suspend fun getPosition(): Int = getData().position.toInt()
    override suspend fun getPermissionOverrides(): List<PermissionOverride> =
        getData().permissionOverrides.values.toList()

    override suspend fun getLastMessage(): Message? =
        getData().getLastMessage()?.lazyEntity
            ?: context.requester.sendRequest(Route.GetChannelMessages(id, limit = 1)).value
                ?.firstOrNull()
                ?.let { context.cache.pushMessageData(it) }
                ?.lazyEntity

    override suspend fun getMessage(messageID: Long): Message? =
        context.cache.get(GetCacheData.Message(messageID, id))?.lazyEntity
            ?: context.requester.sendRequest(Route.GetChannelMessage(id, messageID)).value
                ?.let { context.cache.pushMessageData(it) }
                ?.lazyEntity

    override suspend fun getLastPinTime(): Instant? = getData().lastPinTime
    override suspend fun getTopic(): String = getData().topic
    override suspend fun isNsfw(): Boolean = getData().isNsfw

    override suspend fun send(embed: EmbedBuilder): Message? = getData().send(embed = embed)?.lazyEntity
    override suspend fun sendFile(name: String, data: ByteArray): Message? = getData().sendFile(name, data)?.lazyEntity
    override suspend fun send(text: String, embed: EmbedBuilder?): Message? = getData().send(text, embed)?.lazyEntity

    override suspend fun flowOfMessages(before: Long?, after: Long?, limit: Int?): Flow<Message> =
        getData().flowOfMessages(before, after, limit)

    override suspend fun getWebhooks(): List<Webhook>? = context.requester.sendRequest(Route.GetChannelWebhooks(id))
        .value
        ?.map { it.toEntity(context, getData().guild, getData()) }

    override suspend fun createWebhook(name: String, avatar: AvatarData?): Webhook? = context.requester
        .sendRequest(Route.CreateWebhook(id, name, avatar))
        .value
        ?.toEntity(context, getData().guild, getData())

    /** Checks if this channel is equivalent to the [given object][other]. */
    override fun equals(other: Any?): Boolean = other is GuildNewsChannel && other.id == id
}

/** A channel in which game developers can sell their game on Discord. */
class GuildStoreChannel internal constructor(override val id: Long, override val context: BotClient) : GuildChannel,
    Mentionable {

    private suspend fun getData() = (context.obtainGuildChannelData(id) as? GuildStoreChannelData)
        ?: error("Attempted to get data for a nonexistent guild store channel with ID $id")

    override suspend fun asMention(): String = id.asMention(MentionType.CHANNEL)
    override suspend fun getName(): String = getData().name
    override suspend fun getPosition(): Int = getData().position.toInt()
    override suspend fun getGuild(): Guild = getData().guild.lazyEntity
    override suspend fun getPermissionOverrides(): List<PermissionOverride> =
        getData().permissionOverrides.values.toList()

    /** Checks if this channel is equivalent to the [given object][other]. */
    override fun equals(other: Any?): Boolean = other is GuildStoreChannel && other.id == id
}

/** A Voice Channel (which is found within a [Guild]). */
class GuildVoiceChannel internal constructor(override val id: Long, override val context: BotClient) : GuildChannel {
    private suspend fun getData() = (context.obtainGuildChannelData(id) as? GuildVoiceChannelData)
        ?: error("Attempted to get data for a nonexistent guild voice channel with ID $id")

    override suspend fun getName(): String = getData().name
    override suspend fun getPosition(): Int = getData().position.toInt()
    override suspend fun getGuild(): Guild = getData().guild.lazyEntity
    override suspend fun getPermissionOverrides(): List<PermissionOverride> =
        getData().permissionOverrides.values.toList()

    /**
     * The bitrate of the [GuildVoiceChannel] from 8 Kbps to 96 Kbps; basically how much data should the channel try
     * to send when people speak ([read this for more information](https://techterms.com/definition/bitrate)).
     */
    suspend fun getBitrate(): Int = getData().bitrate

    /**
     * The maximum number of [users][User] allowed in the [VoiceChannel][GuildVoiceChannel] at the same time.
     * The limit can be in the range `1..99`, if set to `0` there is no limit.
     */
    suspend fun getUserLimit(): Int = getData().userLimit.toInt()

    /** Checks if this channel is equivalent to the [given object][other]. */
    override fun equals(other: Any?): Boolean = other is GuildVoiceChannel && other.id == id
}

/** A collapsible channel category (which is found within a [Guild]). */
class GuildChannelCategory internal constructor(override val id: Long, override val context: BotClient) : GuildChannel {
    private suspend fun getData() = (context.obtainGuildChannelData(id) as? GuildChannelCategoryData)
        ?: error("Attempted to get data for a nonexistent guild voice channel with ID $id")

    override suspend fun getName(): String = getData().name
    override suspend fun getGuild(): Guild = getData().guild.lazyEntity
    override suspend fun getPosition(): Int = getData().position.toInt()
    override suspend fun getPermissionOverrides(): List<PermissionOverride> =
        getData().permissionOverrides.values.toList()

    /** Checks if this channel is equivalent to the [given object][other]. */
    override fun equals(other: Any?): Boolean = other is GuildChannelCategory && other.id == id
}

/** Build and Send an [Embed] to the [TextChannel]. Returns the [Message] which was sent or null if it was not sent. */
suspend inline fun TextChannel.send(embed: EmbedBuilder.() -> Unit): Message? = send(EmbedBuilder().apply(embed))

/**
 * Build and Send an [Embed] to the [TextChannel] with additional [text].
 * Returns the [Message] which was sent or null if it was not sent.
 */
suspend inline fun TextChannel.send(text: String, embed: EmbedBuilder.() -> Unit): Message? =
    send(text, EmbedBuilder().apply(embed))

/**
 * Returns a flow of this channel's [Message]s [before] a given [Message] with an optional [limit]
 * @param before The message id to get messages before.
 * @param limit The max number of messages to return. Whole history is returned if not specified.
 * */
suspend fun TextChannel.flowOfMessagesBefore(before: Long?, limit: Int? = null): Flow<Message> =
    flowOfMessages(before = before, limit = limit)

/**
 * Returns a flow of this channel's [Message]s [after] a given [Message] with an optional [limit]
 * @param after The message id to get messages after.
 * @param limit The max number of messages to return. Whole history is returned if not specified.
 * */
suspend fun TextChannel.flowOfMessagesAfter(after: Long, limit: Int? = null): Flow<Message> =
    flowOfMessages(after = after, limit = limit)

/**
 * Returns the channel's history as a flow of [Message]s with an optional [limit]
 * @param limit The max number of messages to return. Whole history is returned if not specified.
 * */
suspend fun TextChannel.flowOfHistory(limit: Int? = null): Flow<Message> =
    flowOfMessagesBefore(getLastMessage()?.id, limit)

/**
 * Returns the channel's history as a flow of [Message]s from start with an optional [limit]
 * @param limit The max number of messages to return. Whole history is returned if not specified.
 * */
suspend fun TextChannel.flowOfHistoryFromStart(limit: Int? = null): Flow<Message> = flowOfMessagesAfter(0, limit)
